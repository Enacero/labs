import { Component, OnInit } from '@angular/core';

import { User } from '../user';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  activeUsers: User[] = [
    {'name': 'alex',  'status': 'active'},
    {'name': 'god',   'status': 'active'},
    {'name': 'peter', 'status': 'active'},
    {'name': 'pavlo', 'status': 'active'},
  ];
  inactiveUsers: User[] = [
    {'name': 'den', 'status': 'inactive'},
    {'name': 'ann', 'status': 'inactive'},
    {'name': 'drager', 'status': 'inactive'},
    {'name': 'boombaster', 'status': 'inactive'},
  ];
  constructor(private counterService: CounterService) { }

  ngOnInit() {
  }

  changeState(user: User) {
    if (user.status === 'active'){
      this.activeUsers = this.activeUsers.filter(function (el){return el.name != user.name;});
      user.status = 'inactive';
      this.inactiveUsers.push(user);
    }
    else {
      this.inactiveUsers = this.inactiveUsers.filter(function (el){return el.name != user.name;});
      user.status = 'active';
      this.activeUsers.push(user);
    }
    this.counterService.increase()
  }

  getCounter(){
    return this.counterService.getCounter();
  }

}
