import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CounterService {
  counter: number;
  constructor() {this.counter = 0; }
  
  getCounter(){
    return this.counter;
  }
  
  increase(){
    this.counter += 1;
  }
}
