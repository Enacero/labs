import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  started: boolean = false;
  events : string[] = [];
  index : number = 0;
  timer;
  constructor() { }

  ngOnInit() {
  }

  AddEvent(){
    this.index += 1;
    let name : string;
    if (this.index %2 == 0){
      name = 'even'
    }
    else{
      name = 'odd'
    }
    this.events.push(name)
  }
  Start(){
    this.started = true;
    let  _this = this;
    this.timer = setInterval(function() {_this.AddEvent();}, 1000);
  }
  Stop(){
    this.started = false;
    clearInterval(this.timer);
    this.events = [];
    this.index = 0;
  }
}
